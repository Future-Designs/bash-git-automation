# bash-git-automation

A simple script to automate the push and pull of various repositories.
You can push several directories to several repositories as you put in to the config.

## Config

Default configuration is made for three different kind of directories.

- shared (push/pull to all repositories)
- protected (push/pull only to "business" repositories)
- private (push/pull only to "private" repositories)

For each directory you want to push/pull you can write the path into th config.
Choose a matching category (private, shared, ...) for each directory.

```
SHARED_DIRECTORIES
    /path/to/directory
PRIVATE_DIRECTORIES
    /path/to/directory
...
```

For each category (private, shared, ...) write the alias (f.e. "origin") into the
 config.

```
SHARED_REPOSITORIES
    origin
PRIVATE_REPOSITORIES
    example
...
```

If you want more categories apply them to the repositories.cfg and register them in the
 allowed_options.sh.
 
## Usage

To push all repositories simply type in shell:

```
bash git_automation.sh push
```

To pull all repositories simply type in shell:

```
bash git_automation.sh pull
```