#!/usr/bin/env bash

# check argument and start
checkOperation()
{
    local arg=$1
    # check if operation valid
    if checkValidOperations $arg "${operations[@]}"
    then
        # start push
        if [ "$arg" == "${operations[0]}" ] # push
        then
            run push
        fi

        # start pull
        if [ "$arg" == "${operations[1]}" ] # pull
        then
            run pull
        fi
    else
        exit
    fi
}

# check argument (pull, push, etc.)
checkValidOperations()
{
    # need arguments input
    args=( "$@" )

    # first argument is operator
    operator="${args[0]}"

    # compare argument-array with operator
    if inArray "${args[@]}"
    then
        # matched argument
        echo -e "\e[32mArgument" "${args[0]}" "valid, starting cue ...\e[0m"
        return 0 # true
    else
        # no matched argument
        echo -e "\e[31mArgument" "${args[0]}" "invalid!\e[0m"
        echo -e "\e[31mValid arguments are:\e[0m"

        unset "args[0]"

        # list valid operators
        for element in "${args[@]}"
        do
            echo -e "\e[31m" $element "\e[0m"
        done
        return 1
    fi
}

# check if element in array
inArray()
{
    # need arguments input
    args=( "$@" )
    local count=0
    singleArgs=1

    # loop over args
    for i in "${args[@]}"
    do
        # skip first argument (identifier)
        if [ "$count" -lt "$singleArgs" ]
        then
            ((count++))
            continue
        fi

        # match identifier with array-elements
        if [ "${args[0]}" == "$i" ]
        then
            return 0 # true
        else
            continue
        fi
    done
    return 1 # false
}

# build config array
fileToArray()
{
    local count=0
    local markerCount=0
    marker="END"

    while read line
    do
        # check for empty lines
        if [ "$line" == "" ]
        then
            continue
        fi

        if inArray $line "${identifiers[@]}"
        then
            lineMarker="YES"
        else
            lineMarker="NO"
        fi

        # put line in array
        if [ "$lineMarker" == "YES" ]
        then
            if [ $markerCount -eq 0 ]
            then
                config[$count]=$line
                ((markerCount++))
            else
                config[$count]=$marker
                ((count++))
                config[$count]=$line
            fi

        else
            config[$count]=$line
        fi

        # increment index
        ((count++))

    done < $file # use external var (file)

    # last END marker
    config[$count]=$marker
}

# extract single config
# first argument Configuration-Identifier f.e. SHARED_DIRECTORIES
# following arguments is formatted config array
extractSingleConfig()
{
    # needs arguments input
    local args=( "$@" )
    # identifier for config
    local identifier="${args[0]}"
    # unset identifier from args
    unset "args[0]"
    # unset singleConfig
    unset singleConfig

    local count=0

    # loop over args
    for i in "${args[@]}"
    do
        # find identifier set var
        if [ "$identifier" == "$i" ]
        then
            local ident="YES"
        fi

        # write array if identifier is reached
        if [ "$ident" == "YES" ]
        then
            # break if marker END is reached
            if [ "$i" == "END" ]
            then
                break
            fi

            # skip identifier-line
            if [ "$identifier" != $i ]
            then
                #echo $i
                singleConfig[$count]=$i
                ((count++))
            fi
        fi
    done
}

# prepared statements for push
gitPreparePush()
{
    echo -e "\e[32mShow Repositorys:\e[0m"
    git remote -v
    echo -e "\e[32mAdd all ...\e[0m"
    git add --all :/
    echo -e "\e[32mCommit all ...\e[0m"
    git commit --all -m "auto commit"
}

# prepared statements for pull
gitPreparePull()
{
    echo -e "\e[32mShow Repositorys:\e[0m"
    git remote -v
}

pushRepositories()
{
    local id=$1
    local path=$2

    case $id in
        shared)
            echo -e "\e[32mRepository is shared, push to following Repositories...\e[0m\n"
            extractSingleConfig SHARED_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPush" $repository "master ...\e[0m"
                git push $repository master
            done
            ;;
        protected)
            echo -e "\e[32mRepository is protected, push to following Repositories...\e[0m\n"
            extractSingleConfig PROTECTED_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPush" $repository "master ...\e[0m"
                git push $repository master
            done
            ;;
        private)
            echo -e "\e[32mRepository is private, push to following Repositories...\e[0m\n"
            extractSingleConfig PRIVATE_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPush" $repository "master ...\e[0m"
                git push $repository master
            done
            ;;
        *)
            echo "Error"
            exit
            ;;
    esac
}

pullRepositories()
{
    local id=$1
    local path=$2

    case $id in
        shared)
            echo -e "\e[32mRepository is shared, pull from following Repositories...\e[0m\n"
            extractSingleConfig SHARED_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPull" $repository "master...\e[0m"
                git pull $repository master
            done
            ;;
        protected)
            echo -e "\e[32mRepository is protected, pull from following Repositories...\e[0m\n"
            extractSingleConfig PROTECTED_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPull" $repository "master\e[0m"
                git pull $repository master
            done
            ;;
        private)
            echo -e "\e[32mRepository is private, pull from following Repositories...\e[0m\n"
            extractSingleConfig PRIVATE_REPOSITORIES "${config[@]}"
            for repository in "${singleConfig[@]}"
            do
                echo -e "\e[32mPull" $repository "master\e[0m"
                git pull $repository master
            done
            ;;
        *)
            echo "Error"
            exit
            ;;
    esac
}

# main function
run()
{
    local operator=$1

    for directory in "${directories[@]}"
    do
        #echo $directory
        extractSingleConfig $directory "${config[@]}"
        for path in "${singleConfig[@]}"
        do
            # enter path
            echo -e "\n\n"
            echo -e "\e[31mEnter Path" $path "\e[0m"
            cd $path

            # fork pull/push
            case $operator in
                push)
                    # prepare git repo (add, commit)
                    gitPreparePush
                    ;;
                pull)
                    # prepare (show repos)
                    gitPreparePull
                ;;
                *)
                    echo "Error run() in functions.sh Operator not matching!"
                    exit
                ;;
            esac
            routeRepositories $operator $directory $path

        done
    done
}

# matching repos to directories
# arg1 directory, arg2 path
routeRepositories()
{
    local operator=$1
    local dir=$2
    local path=$3

    if [ "$operator" == "push" ]
    then
        case $dir in
            SHARED*)
                pushRepositories shared $path
                ;;
            PROTECTED*)
                pushRepositories protected $path
                ;;
            PRIVATE*)
                pushRepositories private $path
                ;;
            *)
                echo "No matching repositories for" $arg "Directory"
                exit
                ;;
        esac
    fi

    if [ "$operator" == "pull" ]
    then
        case $dir in
            SHARED*)
                pullRepositories shared $path
                ;;
            PROTECTED*)
                pullRepositories protected $path
                ;;
            PRIVATE*)
                pullRepositories private $path
                ;;
            *)
                echo "No matching repositories for" $arg "Directory"
                exit
                ;;
        esac
    fi
}