#!/usr/bin/env bash

# source file for repo-list
file="config/repositories.cfg"

# allowed config sections
identifiers=(
SHARED_DIRECTORIES
PROTECTED_DIRECTORIES
PRIVATE_DIRECTORIES
SHARED_REPOSITORIES
PROTECTED_REPOSITORIES
PRIVATE_REPOSITORIES
)

# kind of directories
directories=(
SHARED_DIRECTORIES
PROTECTED_DIRECTORIES
PRIVATE_DIRECTORIES
)

# kind of repositories
repositories=(
SHARED_REPOSITORIES
PROTECTED_REPOSITORIES
PRIVATE_REPOSITORIES
)

# allowed operations
operations=(
push
pull
)